import { NextApiRequest, NextApiResponse } from "next";

const helloApi = (_: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json({ text: "Hello" });
};

export default helloApi;
